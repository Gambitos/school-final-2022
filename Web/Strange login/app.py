# save this as app.py
import string
from flask import Flask, render_template, session, request
import os
import random
import secrets

app = Flask(__name__)
secret = secrets.token_urlsafe(16)
app.config['SECRET_KEY'] = secret 

def newCookie():
    cookie = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(25))
    session['data'] = cookie
    print(session['data'])


@app.route("/", methods=["GET","TRACE"])
def home():
    if not session.get("data"):
        newCookie()
    return render_template('home.html')

@app.route("/admin", methods=["GET","TRACE"])
def admin():
    if not session.get("data"):
        newCookie()
    headersa = dict(request.headers) 
    if 'X-Ip-For-Authentication' not in request.headers:
        headersa['X-Ip-For-Authentication'] = str(request.remote_addr)
    if request.method == "GET":
        if headersa['X-Ip-For-Authentication'] == str('127.0.0.1'):
            return 'API Key is: 4a5455304a5455314a54526c4a5455314a5455794a545a6c4a5463304a5455304a5455784a5451334a544d784a5451324a5459304a54517a4a5451324a5463304a54526b4a544d784a54526c4a5459324a5455324a5451344a5452684a5451784a5455784a544d794a5455324a5459324a5451354a5455344a54526c4a5459324a5455784a5451344a5459304a5451784a5459784a5455334a5463344a5451784a5455784a5459354a5451314a5464684a5459324a5455784a544e6b4a544e6b'
        else:
            return "You should be admin to see it!"
    elif request.method == "TRACE":
        return headersa



if __name__ == "__main__":
    app.run(debug=False, host=os.environ['IP'], port=os.environ['PORT'])

