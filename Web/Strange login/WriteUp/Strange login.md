# Writeup "Strange login"

1) We go to the site, and we see the inscription that the site is under development ...
And in fact, there is nothing on this page

![image](./images/Screenshot_1.png)

2) Let's iterate over the directories. And we will find the /admin page. Let's go there!

![image](./images/Screenshot_2.png)

3) The site does not allow us to see the content of the /admin page :(

![image](./images/Screenshot_3.png)

4) Let's intercept a GET request to the admin page and see all available HTTP methods. And we see that the TRACE method is available to us.

![image](./images/Screenshot_4.png)

5) If we make a TRACE request, we will see that the site displays the request received from us. If you look at it, you can see an interesting header - X-Ip-For-Authentication. The value of which is our ip. Maybe we need to somehow become 127.0.0.1?

![image](./images/Screenshot_5.png)

6) Go to the next tabs in burp, and find the function 'Match and Replace'. Press the add button.

![image](./images/Screenshot_6.png)

7) We write the following in the 'Replace' line: X-Ip-For-Authentication: 127.0.0.1. Then click OK and update the /admin page.

![image](./images/Screenshot_7.png)

8) We see that the API Key has appeared. This is not a flag!!! It still needs to be decoded. For this purpose, I will use burp.

![image](./images/Screenshot_8.png)

9) Let's decode the API Key in the following way: ascii hex -> base64 -> url -> base64 -> get the flag :)

![image](./images/Screenshot_9.png)
