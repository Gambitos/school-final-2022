# Writeup "Rough website v2"

GREAT RETURN!
Just kidding, who cares, let's just solve this task

1) We go to the site and see almost the same thing as last time, only the site has become a little gloomier.

![image](./images/Screenshot_1.png)

2) Let's substitute quotes and implement sqli payloads.
Uh, nothing works, the site calmly handles all this.

![image](./images/Screenshot_2.png)

3) And let's look at the html of the first page, maybe we missed something. And it's true, we missed this note:

![image](./images/Screenshot_3.png)

It says here that the debug mode, which runs as the admin user should be removed.....

4) Let's intercept the POST request and add the following parameters: debug=true&user&admin. And also put a quote in the login.

And we will get an error, so we can try to call sqli.

![image](./images/Screenshot_4.png)

5) Copy all successful request

![image](./images/Screenshot_5.png)

6) Create a rec file and edit it

![image](./images/Screenshot_6.png)

7) Paste the copied request into the file and save it

![image](./images/Screenshot_7.png)

8) Run sqlmap, in which we specify the query from the rec file, also set the keys --dump, --risk 3 --level 5

![image](./images/Screenshot_8.png)

9) And sqlmap gives us an error because I forgot to remove the quote at the end of login. We return to the created file, remove the quote, save and run sqlmap again.

![image](./images/Screenshot_9.png)

10) Sqlmap found and displayed all the data in the users table. But there is no flag.

![image](./images/Screenshot_10.png)

11) Also sqlmap found the Flags table with several false flags and 1 correct one. Congratulations!

![image](./images/Screenshot_11.png)
