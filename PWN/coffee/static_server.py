import os
from flask import Flask, Response
from flask import send_from_directory

app = Flask(__name__)

# @app.route('/ok')
# def ok():
#     return 'ok'

@app.route('/')
def send_binary():
    app.logger.info('downloaded binary')
    return send_from_directory("./app/", os.getenv('BIN_NAME'))

if __name__ == '__main__':
    app.run(host="localhost", port=8080)
