# FalseCrypt

The criminal did not give up any of his secrets, but we did manage to get some hdd and freezed RAM...
We need proof of his guilt

Download: https://disk.yandex.ru/d/2ZnFE7_gTIyGDA 

P.S.: How to [unpack *.zst archives](https://github.com/facebook/zstd/)

## Writeup

We provided with RAM file and *data_of_criminal.bin*
Bin file is encrypted TrueCrypt container.
Using volatility2 (as v3 do not include plugins to work with TC) we can try to get information about container from RAM:

    docker pull blacktop/volatility # Pulling cont with vol2
    docker run --rm -v $(pwd):/data:ro blacktop/volatility -f criminal_ram.bin imageinfo # Here we can see that it is WIN7`s RAM
    docker run --rm -v $(pwd):/data:ro blacktop/volatility -f criminal_ram.bin --profile=Win7SP1x64 truecryptsummary

After this command we can see that criminal was quite incompetent coz turned ON "Cache passwords" option
PASSWORD: bQmzx8UKErZSmPRAmTejNPBXn5xIVe

Next we use this password and TrueCrypt program to open *data_of_criminal.bin* and get the flag

## Difficulty
Easy

# 

## Flag
MCTF{CrIm1naL-wi11_be_cAughT}

