# The Best Editor
## Сложность 
Medium 

## Опсиание 
Вы когда-ниубдь слышали о виме? В нём очень много функций, можете проверить!
http://ip:8016

## Writeup
Подключаемся к порту и видим *не совсем* vim. 
```
nc localhost 8016
```
![image](./images/start.png)

Выходить из него не надо(хехе), лучше заспауним оболочку.
```
:!/bin/bash
```
Мы получаем оболочку под пользователем vimuser(uid 1000). Помимо этого, у нас есть УЗ admin(uid 1001), запомним. 
![image](./images/users.png)

Одна из основных тактик повышения привилегий это поиск [suid](https://ru.wikipedia.org/wiki/Suid) битов. Они позволяют выполнять файлы от uid владельца. Их можно найти командой
```
find /usr/bin -perm -4000
```
Мы видим что у нас есть ещё одна binary Vim'a, у которой есть suid бит, а принадлежит она пользователю admin
![image](./images/privs.png)

Её можно запустить, однако при попытке заспаунить shell через него, у нас останется uid vimuser. Однако для просмотра файлов через Vim можно использовать его внутренний браузер файлов. 
Логичный вариант - открыть домашнюю папку пользователя admin. 
```
vim /home/admin/
```
![image](./images/home.png)

И вот он, флаг. Можно заркыть vim, и повторно отркыть командой *vim /home/admin/flag.txt* или, не выходя из эксплорера
```
:e /home/admin/flag.txt
```
![image](./images/flag.png)
## Hint
Тебе не нужна оболочка, чтобы делать обычные вещи.
## Flag
```
MCTF{Unl1mited_V1m_P0wer}
```

## Hint 
1) Для таких простых действий и оболочка не нужна
